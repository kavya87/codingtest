﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodingTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfLetters = 0;
            for (int number = 1; number < 1001; number++)
                numberOfLetters += CountLetters(number);
            Console.WriteLine("Number of letters: " + numberOfLetters);
            Console.ReadKey();
        }

        static int CountLetters(int number)
        {
            string strNumber = "";
            string[] strDigits = new string[] { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
            string[] strTeens = new string[] { "", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
            string[] strTens = new string[] { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

            if (number == 1000)
                return "onethousand".Length;

            int[] intDigits = new int[3];

            intDigits[0] = number / 100;
            intDigits[1] = (number / 10) % 10;
            intDigits[2] = number % 10;

            if (intDigits[0] > 0)
            {
                strNumber += strDigits[intDigits[0]] + "hundred";
                if (intDigits[1] != 0 || intDigits[2] != 0)
                    strNumber += "and";
            }

            if (intDigits[1] == 1)
            {
                if (intDigits[2] == 0)
                    strNumber += strTens[intDigits[1]];
                else
                    strNumber += strTeens[intDigits[2]];
            }
            else
                strNumber += strTens[intDigits[1]] + strDigits[intDigits[2]];

            return strNumber.Length;

        }
    }
}

